package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"time"
)

func readNext(gotOne chan struct{}, gotError chan error, gotDone chan struct{}) {
	reader := bufio.NewReader(os.Stdin)
	for {
		_, err := reader.ReadString('\n')
		if err == io.EOF {
			gotDone <- struct{}{}
			return
		}
		if err != nil {
			gotError <- err
			return
		}
		gotOne <- struct{}{}
	}
}

func main() {
	lines := 0
	gotOne := make(chan struct{})
	gotError := make(chan error)
	gotDone := make(chan struct{})
	go readNext(gotOne, gotError, gotDone)
	for {
		select {
		case <-gotOne:
			lines++
		case <-time.After(250 * time.Millisecond):
			if lines > 0 {
				fmt.Printf("%v\n", lines)
				lines = 0
			}
		case <-gotDone:
			fmt.Printf("%v\n", lines)
			os.Exit(0)
		case err := <-gotError:
			fmt.Fprintf(os.Stderr, "unable to continue: %v\n", err)
			os.Exit(1)
		}
	}
}
