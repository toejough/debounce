# Debounce

A simple shell pipeline debounce utility.

## What it does

1. Consumes lines from stdin until there haven't been any new inputs in 1/4 seconds.
1. At that moment, it prints the count of lines consumed to stdout on a new line.
1. Repeat.

## How to use it

`<noisy command> | debounce | <command to run after noisy command settles>`

## Example

* Watch the current directory for changes with `fswatch.`.  
* Save 5 files at once.
* Echo what files changed.
* Run your `go test` command once

`fswatch . | tee /dev/fd/2 | debounce | xargs -L1 -I% go test`

## Disclaimer

This utility was written to scratch an itch, and for the moment, it does.  I'm
not sure when or if I'll have the occassion or need to improve it, but I'd love
to hear if you find it useful and would like more features.